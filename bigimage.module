<?php

/**
 * Two simple functions, one to theme a map, and the other as a callback for
 * the tile for an image.
 */
/**
 * Implementation of hook_menu().
 */
function bigimage_menu(){
  return array(
    'bigimage/%/%/%/%' => array(
      'title' => 'Big Image Callback',
      'title callback' => FALSE,
      'page callback' => 'bigimage_get_tile',
      'page arguments' => array(
        1, // FID
        2, // Zoom
        3, // x
        4 //  y
      ),
      'access callback' => TRUE
    )
  );
}

/**
 * Implementation of hook_field_widget_info().
 */
function bigimage_field_widget_info(){
  return array(
    'bigimage_annotations_widget' => array(
      'label' => t('Image and annotations'),
      'description' => t('Add anotations to small or BIG images.'),
      'field types' => array(
        'bigimage_annotations'
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_NONE
      )
    )
  );
}

/**
 * Implementation of hook_field_info().
 */
function bigimage_field_info(){
  return array(
    'bigimage_annotations_field' => array(
      'label' => t('Image + annotations'),
      'description' => t('This field stores an image, along with annotations for the image.'),
      'default_widget' => 'bigimage_annotations_widget',
      'default_formatter' => 'bigimage_annotations_formatter'
    )
  );
}

/**
 * Implementation of hook_field_formatter_info().
 */
function bigimage_field_formatter_info(){
  return array(
    'bigimage_bigimage_formatter' => array(
      'label' => t('Bigimage'),
      'description' => t('Displays a bigmiage as a tile.'),
      'field types' => array(
        'image'
      ),
      'settings' => array(
        'minimum_dimensions' => '500',
        'image_style' => ''
      )
    ),
    'bigimage_annotations_formatter' => array(
      'label' => t('Image + annotations'),
      'description' => t('Displays an image along with its annotations.'),
      'field types' => array(
        'bigimage_annotations_field'
      )
    )
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function bigimage_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state){
  return array(
    'minimum_dimensions' => array(
      '#title' => t('Minimum dimensions'),
      '#type' => 'select',
      '#default_value' => $instance['display'][$view_mode]['settings']['minimum_dimensions'],
      '#empty_option' => t('Always use'),
      '#options' => array(
        500 => 500,
        600 => 600,
        700 => 700,
        800 => 800,
        1000 => 1000,
        1200 => 1200,
        1400 => 1400,
        1600 => 1600,
        2000 => 2000,
        3000 => 3000,
        5000 => 5000
      )
    ),
    'image_style' => array(
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $instance['display'][$view_mode]['settings']['image_style'],
      '#empty_option' => t('None (original image)'),
      '#options' => image_style_options(FALSE)
    )
  );
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function bigimage_field_formatter_settings_summary($field, $instance, $view_mode){
  switch($instance['display'][$view_mode]['type']){
    case 'bigimage_bigimage_formatter':
      $summary = array();
      if(!isset($instance['display'][$view_mode]['settings']['minimum_dimensions']) || !$instance['display'][$view_mode]['settings']['minimum_dimensions']){
        $summary[] = t('Always use bigimage.');
      }else{
        $this_summary = t('Images smaller than %dimensions px will be displayed as: ', array(
          '%dimensions' => $instance['display'][$view_mode]['settings']['minimum_dimensions']
        ));
        if(isset($instance['display'][$view_mode]['settings']['image_style']) && $instance['display'][$view_mode]['settings']['image_style']){
          $image_styles = image_style_options(FALSE);
          $this_summary .= t('"Image style @style"', array(
            '@style' => $image_styles[$instance['display'][$view_mode]['settings']['image_style']]
          ));
        }else{
          $this_summary .= t('Original image');
        }
        $summary[] = $this_summary;
      }
      return implode('<br />', $summary);
  }
}

/**
 * Implements hook_field_formatter_view().
 * 
 * FIXME - Still need to do the field/entity formats properly (so that an entity
 * map will show all the fields set as "entity_map" on that entity.
 */
function bigimage_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display){
  $elements = array();
  switch($display['type']){
    case 'bigimage_bigimage_formatter':
      $min_dimension = isset($display['settings']['minimum_dimensions']) ? $display['settings']['minimum_dimensions'] : 0;
      foreach($items as $delta => $item){
        if($item['height'] > $min_dimension || $item['width'] > $min_dimension){
          $elements[$delta] = theme('bigimage', array(
            'fid' => $item['fid']
          ));
        }else{
          $elements[$delta] = array(
            '#theme' => 'image_formatter',
            '#item' => $item,
            '#image_style' => $display['settings']['image_style']
          );
        }
      }
      break;
  }
  return $elements;
}

/**
 * Implementation of hook_theme().
 */
function bigimage_theme(){
  return array(
    'bigimage' => array(
      'variables' => array(
        'fid' => '0'
      )
    )
  );
}

/**
 * Batch function for creating the tiles.
 */
function bigimage_batch_process($fid, &$context){
  if(!isset($context['sandbox']['total_tiles'])){
    // Calculate the total number of tiles for this file.
    $file = file_load($fid);
    // By making this directory here, we ensure that we don't end up with a loop
    // of batch processes.
    drupal_mkdir("public://bigimage/$fid");
    $image = image_load($file->uri);
    $max_dimension = $image->info['width'] > $image->info['height'] ? $image->info['width'] : $image->info['height'];
    $max_zoom = ceil(log($max_dimension / 256) / log(2));
    $context['sandbox']['total_tiles'] = _bigimage_tiles_per_zoom($max_zoom);
    $context['sandbox']['images_processed'] = 0;
  }
  $batch = batch_get();
  $tiles_processed_this_round = 0;
  _bigimage_create_tile($fid, $context['sandbox']['images_processed']);
  $context['sandbox']['images_processed']++;
  $context['message'] = t('Processed %processed_images of %max_images tiles', array(
    '%processed_images' => $context['sandbox']['images_processed'],
    '%max_images' => $context['sandbox']['total_tiles']
  )) . '<br/>' . t('Estimated time remaining: %time_remaining', array(
    '%time_remaining' => format_interval(($batch['sets'][0]['elapsed'] * ($context['sandbox']['total_tiles'] / $context['sandbox']['images_processed']) - $batch['sets'][0]['elapsed']) / 1000)
  ));
  if($context['sandbox']['images_processed'] > 341){
    $context['message'] .= '<br/>' . l(t('The first four levels have been created, it is safe to skip creating the rest, and to let them be created dynamically.'), $batch['source_url']);
  }
  $context['finished'] = $context['sandbox']['images_processed'] / $context['sandbox']['total_tiles'];
}

/**
 * Create an image tile.
 */
function _bigimage_create_tile($fid, $image_number){
  // We need to calculate the zoom level for the image number.
  $zoom_level = 0;
  while(true){
    if($image_number < _bigimage_tiles_per_zoom($zoom_level)){
      break;
    }
    $zoom_level++;
  }
  // Once we have the zoom level, we need to know the number within that zoom
  // level, and also calculate the x and y.
  if($zoom_level > 0){
    $image_number_per_level = $image_number - _bigimage_tiles_per_zoom($zoom_level - 1);
  }
  $zoom_level_dimensions = pow(2, $zoom_level);
  if($image_number){
    $x = floor($image_number_per_level / $zoom_level_dimensions);
    $y = $image_number_per_level % $zoom_level_dimensions;
    bigimage_create_tile($fid, $zoom_level, $x, $y);
  }else{
    bigimage_create_tile($fid, 0, 0, 0);
  }
}

/**
 * Function to calculate the total number of tiles required for a zoom level.
 */
function _bigimage_tiles_per_zoom($zoom, $recursive = TRUE){
  if($recursive){
    if($zoom == 0){return 1;}
    return pow(pow(2, $zoom), 2) + _bigimage_tiles_per_zoom($zoom - 1);
  }else{
    return pow(pow(2, $zoom), 2);
  }
}

/**
 * Theme function.
 */
function theme_bigimage($variables){
  // Check to see if this file has been displayed before.
  if(!file_exists('public://bigimage/' . $variables['fid'])){
    // If it hasn't, then we create a batch, redirect to the batch page, and
    // then redirect back to this original page.
    $batch = batch_set(array(
      'operations' => array(
        array(
          'bigimage_batch_process',
          array(
            $variables['fid']
          )
        )
      ),
      'title' => t('Tiling image')
    ));
    batch_process();
  }else{
    $file = file_load($variables['fid']);
    $image = image_load($file->uri);
    $max_dimension = $image->info['width'] > $image->info['height'] ? $image->info['width'] : $image->info['height'];
    $max_zoom = ceil(log($max_dimension / 256) / log(2));
    $max_dimension = pow(2, $max_zoom) * 256;
    // Longitude is easy - linear.
    $longitude = (($image->info['width'] / ($max_dimension * 2)) * 360) - 180;
    // Latitude - Mercator complications.  Thanks to the code from 
    // http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#tile_numbers_to_lon.2Flat_4
    $latitude = rad2deg(atan(sinh(pi() * (1 - ($image->info['height'] / $max_dimension)))));
    $image = null;
    return theme('gm3_map', array(
      'map' => array(
        'id' => 'bigimage-' . $variables['fid'],
        'settings' => array(
          'zoom' => 2,
          'minZoom' => 2,
          'maxZoom' => $max_zoom,
          'center' => array(
            'latitude' => $latitude,
            'longitude' => $longitude
          ),
          'mapTypeControlOptions' => array(
            'mapTypeIds' => array()
          )
        ),
        'libraries' => array(
          'bigimage' => array(
            'fid' => $variables['fid'],
            'module' => 'bigimage'
          ),
          'polygon' => array(
            'polygons' => array(
              array(
                array(
                  'lat' => 78.41247213570998,
                  'long' => -20.418090820312045
                ),
                array(
                  'lat' => 76.90195383437998,
                  'long' => -20.418090820312045
                ),
                array(
                  'lat' => 76.90195383437998,
                  'long' => -11.365356445312045
                ),
                array(
                  'lat' => 78.41247213570998,
                  'long' => -11.365356445312045
                )
              )
            )
          )
        )
      )
    ));
  }
}

/**
 * Implementation of hook_library().
 */
function bigimage_library(){
  return array(
    'bigimage' => array(
      'title' => t('Google Maps Javascript API V3: Big image'),
      'website' => 'http://code.google.com/apis/maps/',
      'version' => '3',
      'js' => array(
        array(
          'data' => drupal_get_path('module', 'bigimage') . "/js/bigimage.js"
        ),
        array(
          'data' => array(
            'bigimage' => array(
              'callback' => url('bigimage')
            )
          ),
          'type' => 'setting'
        )
      ),
      'dependencies' => array(
        array(
          'gm3',
          'gm3'
        )
      )
    )
  );
}

/**
 * Callback for the above menu function.
 */
function bigimage_get_tile($fid, $zoom, $x, $y){
  if(bigimage_create_tile($fid, $zoom, $x, $y)){
    drupal_goto(file_create_url("public://bigimage/$fid/$zoom/$x-$y.jpg"), array(), 301);
  }else{
    bigimage_get_tile_error();
  }
}

/**
 * Does the heavy lifting for the callback, and the batch function.
 * 
 * Returns TRUE on success (or if it already exists), and FALSE on failure.
 */
function bigimage_create_tile($fid, $zoom, $x, $y){
  // Added for debugging.
  return TRUE;
  // Create the fid folder.
  if(!file_exists("public://bigimage/$fid") || !file_exists("public://bigimage/$fid/base.jpg")){
    // Load the file object
    $file = file_load($fid);
    if(!$file){
      // WTF! Error dude!
      watchdog('bigimage', 'We tried to load a file and failed');
      return FALSE;
    }
    // Create the directory (therefore, if this directory is there, we know the
    // fid is valid).
    drupal_mkdir("public://bigimage/$fid");
    $image = image_load($file->uri);
    $max_dimension = $image->info['width'] > $image->info['height'] ? $image->info['width'] : $image->info['height'];
    $max_zoom = ceil(log($max_dimension / 256) / log(2));
    $max_dimension = pow(2, $max_zoom) * 256;
    // Next we need to create a base image that is padded to make cropping and
    // scaling much easier.
    image_crop($image, 0, 0, $max_dimension, $max_dimension);
    image_save($image, "public://bigimage/$fid/base.jpg");
  }
  // Create the zoom level folder.
  if(!file_exists("public://bigimage/$fid/$zoom")){
    drupal_mkdir("public://bigimage/$fid/$zoom");
  }
  // Next create the zoomed image based on the max_zoom, and the base image
  if(!file_exists("public://bigimage/$fid/$zoom/base.jpg")){
    // Zoom of 0 requires the image to be scaled to 256*256, Zoom of 1 requires
    // the image to be scaled to 512*512, Zoom of 2 requires the image to be
    // scaled to 1024*1024, and so on.
    $image = image_load("public://bigimage/$fid/base.jpg");
    image_scale($image, 256 * pow(2, $zoom));
    image_save($image, "public://bigimage/$fid/$zoom/base.jpg");
  }
  // Finally, create the crop of the image if we don't already have it.
  if(!file_exists("public://bigimage/$fid/$zoom/$x-$y.jpg")){
    // Check that we have a valid x and y
    if($x < 0 || $y < 0 || $x >= pow(2, $zoom) || $y >= pow(2, $zoom)){return FALSE;}
    // We serve the file, else we go through a sequence of events to prepare 
    // the file.
    $image = image_load("public://bigimage/$fid/$zoom/base.jpg");
    // FIXME - Check if $x and $y start at 1 or 0
    image_crop($image, ($x * 256) + 1, ($y * 256) + 1, 256, 256);
    image_save($image, "public://bigimage/$fid/$zoom/$x-$y.jpg");
  }
  // Redirect permanently to the file, allowing Apache to work its magic (or
  // whatever web server you're using).
  return TRUE;
}

/**
 * Error tile.
 */
function bigimage_get_tile_error(){
  // Return a standard black tile.
  drupal_goto(file_create_url(drupal_get_path('module', 'bigimage') . '/empty.jpg'), array(), 301);
}
